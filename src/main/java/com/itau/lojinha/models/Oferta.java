package com.itau.lojinha.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;

public class Oferta {
	
	@PrimaryKey
	private UUID id;
	@NotNull
	private String texto;
	@NotNull
	private double preco;
	@NotNull
	private String anuncioId;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public String getAnuncioId() {
		return anuncioId;
	}
	public void setAnuncioId(String anuncioId) {
		this.anuncioId = anuncioId;
	}
	

}
