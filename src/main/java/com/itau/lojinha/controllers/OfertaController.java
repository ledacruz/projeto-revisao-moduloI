package com.itau.lojinha.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.lojinha.models.Oferta;
import com.itau.lojinha.repositories.OfertasRepository;

@RestController
@RequestMapping("/ofertas")
public class OfertaController {
	
	@Autowired
	OfertasRepository ofertasRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public Oferta criar(@RequestBody Oferta ofertas) {
		ofertas.setId(UUID.randomUUID());
		return ofertasRepository.save(ofertas);
	}
	

}
