package com.itau.lojinha.controllers;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.lojinha.models.Anuncio;
import com.itau.lojinha.models.Usuario;
import com.itau.lojinha.repositories.AnunciosRepository;
import com.itau.lojinha.repositories.UsuarioRepository;
import com.itau.lojinha.service.JWTService;

@RestController
@RequestMapping("/anuncio")
public class AnuncioController {
	@Autowired
	AnunciosRepository anunciosRepository;
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	JWTService jwtService;

	@RequestMapping(method = RequestMethod.POST)
	public Anuncio criar(@RequestHeader(value="Authorization") String authorization, @RequestBody Anuncio anuncio) {
		
		String token = authorization.substring(7,120);
		
		Optional<Usuario> usuario = usuarioRepository.findByUsername(jwtService.validarToken(token));
		
		anuncio.setUsername(usuario.get().getUsername());
		anuncio.setId(UUID.randomUUID());
		return anunciosRepository.save(anuncio);
				
	}
	
	/*@RequestMapping(method = RequestMethod.POST)
	public Anuncio criar(@RequestHeader String aunonethorization, @RequestBody Anuncio anuncio) {
		
		Optional<Usuario> usuario = usuarioRepository.findByUsername(jwtService.validarToken(authorization));
		
		anuncio.setUsername(usuario.get().getUsername());
		anuncio.setId(UUID.randomUUID());
		return anunciosRepository.save(anuncio);
				
	}*/

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<?> getAnuncios() {
		return anunciosRepository.findAll();
	}
	
	

}
