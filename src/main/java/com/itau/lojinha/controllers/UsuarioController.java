package com.itau.lojinha.controllers;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.lojinha.models.Login;
import com.itau.lojinha.models.Usuario;
import com.itau.lojinha.repositories.LoginRepository;
import com.itau.lojinha.repositories.UsuarioRepository;
import com.itau.lojinha.service.PasswordService;

@RestController
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	LoginRepository loginRepository;
	
	
	@Autowired
	PasswordService passwordService;

	@RequestMapping(method = RequestMethod.POST, path = "/usuario")
	public Usuario criar(@RequestBody Usuario usuario) {

		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);

		Login login = new Login();
		
		login.setUsername(usuario.getUsername());
		login.setSenha(usuario.getSenha());
		
		loginRepository.save(login);
		return usuarioRepository.save(usuario);
	}
}
