package com.itau.lojinha.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.lojinha.models.Login;
import com.itau.lojinha.models.Usuario;
import com.itau.lojinha.repositories.UsuarioRepository;
import com.itau.lojinha.service.JWTService;
import com.itau.lojinha.service.PasswordService;

@RestController
public class LoginController {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	PasswordService passwordService;
	
	@Autowired
	JWTService jwtService;
	
	@RequestMapping(method = RequestMethod.POST, path = "/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Login login) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByUsername(login.getUsername());

		if (!usuarioBancoOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		Usuario usuarioBanco = usuarioBancoOptional.get();

		if (passwordService.verificarHash(login.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco.getUsername());

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");

			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);
		}else {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Access-Control-Expose-Headers", "Failed");


		return ResponseEntity.badRequest().build();
		}
	}
}
