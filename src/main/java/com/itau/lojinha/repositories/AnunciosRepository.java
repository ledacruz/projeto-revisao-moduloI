package com.itau.lojinha.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.lojinha.models.Anuncio;

public interface AnunciosRepository extends CrudRepository<Anuncio, UUID>{

}
