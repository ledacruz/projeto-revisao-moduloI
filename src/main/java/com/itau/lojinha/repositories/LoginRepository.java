package com.itau.lojinha.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.lojinha.models.Login;

public interface LoginRepository extends CrudRepository<Login, Login>{
	
}
