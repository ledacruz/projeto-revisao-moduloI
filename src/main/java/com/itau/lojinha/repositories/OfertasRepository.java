package com.itau.lojinha.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.lojinha.models.Oferta;

public interface OfertasRepository extends CrudRepository<Oferta, UUID>{

}
