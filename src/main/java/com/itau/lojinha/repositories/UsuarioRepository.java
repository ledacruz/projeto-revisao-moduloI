package com.itau.lojinha.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.lojinha.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, UUID>{

	public Optional<Usuario> findByUsername(String username);
}
